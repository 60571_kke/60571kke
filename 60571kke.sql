-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Мар 25 2021 г., 23:56
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571kke`
--

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Структура таблицы `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '127.0.0.1', '89527152665', 1616677685);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$12$eD6ctuJtAvSdC4zmMRUXV.8zVhI6SLKnbNI8KVY8QuMNdftQaI3Hi', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1616677776, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '127.0.0.1', 'kozhikhova_ksenia@mail.ru', '$2y$10$j0Sy3XlDxuASJmy2yD8jEORCDF2ndNaEudvGd7cfewQSbzAh9/5L2', 'kozhikhova_ksenia@mail.ru', NULL, NULL, NULL, NULL, NULL, 'b61a055fea275605328c62decd3d14c81ce0dad7', '$2y$10$YCNcZfE.rFhmJEow/lgnCeKeVZxa/805yjFvAJZCYCLi2tHNI2Kzq', 1616677677, 1616677710, 1, 'Ксения', 'Кожихова', 'СурГУ', '89527152665');

-- --------------------------------------------------------

--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `заказ`
--

CREATE TABLE `заказ` (
  `id` int NOT NULL,
  `id_покупателя` int NOT NULL,
  `дата_и_время_отправки` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `заказ`
--

INSERT INTO `заказ` (`id`, `id_покупателя`, `дата_и_время_отправки`) VALUES
(1, 1, '2021-02-25 12:57:49'),
(2, 2, '2021-02-25 14:46:27'),
(3, 3, '2021-02-25 16:05:46'),
(4, 4, '2021-02-25 17:26:34'),
(5, 5, '2021-02-25 18:01:20'),
(6, 6, '2021-02-25 18:54:03'),
(7, 6, '2021-02-25 19:39:14'),
(8, 7, '2021-02-26 09:16:49'),
(9, 8, '2021-02-26 10:28:15'),
(10, 9, '2021-02-26 11:41:09'),
(11, 10, '2021-02-26 12:30:57');

-- --------------------------------------------------------

--
-- Структура таблицы `категория_товара`
--

CREATE TABLE `категория_товара` (
  `id` int NOT NULL,
  `наименование` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `категория_товара`
--

INSERT INTO `категория_товара` (`id`, `наименование`) VALUES
(1, 'Макияж'),
(2, 'Уход за лицом'),
(3, 'Уход за волосами'),
(4, 'Уход за телом'),
(5, 'Парфюмения'),
(6, 'Маникюр и педикюр'),
(7, 'Гигиена полости рта');

-- --------------------------------------------------------

--
-- Структура таблицы `оплата`
--

CREATE TABLE `оплата` (
  `id` int NOT NULL,
  `id_покупателя` int NOT NULL,
  `дата_и_время_оплаты` datetime NOT NULL,
  `сумма, ₽` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `оплата`
--

INSERT INTO `оплата` (`id`, `id_покупателя`, `дата_и_время_оплаты`, `сумма, ₽`) VALUES
(1, 1, '2021-02-25 09:03:44', 433),
(2, 2, '2021-02-25 11:18:06', 536),
(3, 3, '2021-02-25 12:58:16', 2925),
(4, 4, '2021-02-25 14:01:49', 555),
(5, 5, '2021-02-25 15:26:05', 395),
(6, 6, '2021-02-25 15:59:01', 299),
(7, 6, '2021-02-25 16:02:16', 497),
(8, 7, '2021-02-25 18:17:59', 490),
(9, 8, '2021-02-25 20:38:34', 392),
(10, 9, '2021-02-25 23:01:50', 499),
(11, 10, '2021-02-26 02:16:24', 599);

-- --------------------------------------------------------

--
-- Структура таблицы `покупатель`
--

CREATE TABLE `покупатель` (
  `id` int NOT NULL,
  `ФИО` varchar(255) NOT NULL,
  `адрес_доставки` varchar(255) NOT NULL,
  `e-mail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `покупатель`
--

INSERT INTO `покупатель` (`id`, `ФИО`, `адрес_доставки`, `e-mail`) VALUES
(1, 'Савина А.Н.', 'ул. Пушкина д.17 кв.99', 'savina_an@pochta.ru'),
(2, 'Зайцева В.А.', 'ул. Островского д.18 кв.23', 'zayceva_va@pochta.ru'),
(3, 'Серебряков В.С.', 'ул. Бажова д.18 кв.79', 'serebryakov_vs@pochta.ru'),
(4, 'Масленникова Н.В.', 'ул. Военная д.10 кв.21', 'maslennikova_nv@pochta.ru'),
(5, 'Горелова М.Е.', 'ул. Гагарина д.19 кв.61', 'gorelova_me@pochta.ru'),
(6, 'Дружинина О.Я.', 'ул. Высоцкого д.19 кв.38', 'drukzhinina_oya@pochta.ru'),
(7, 'Большакова Е.А.', 'ул. Гоголя д.18 кв.9', 'bolshakova_ea@pochta.ru'),
(8, 'Антипова А.А.', 'ул. Зимняя д.12 кв.1', 'antipina_aa@pochta.ru'),
(9, 'Субботина К.Г.', 'ул. Московская д.11 кв.47', 'subbotina_kg@pochta.ru'),
(10, 'Яшина О.Ю.', 'ул. Гражданская д.164 кв.5', 'yashina_oyu@pochta.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `товар`
--

CREATE TABLE `товар` (
  `id` int NOT NULL,
  `наименование` varchar(255) NOT NULL,
  `id_категории` int NOT NULL,
  `вес` float NOT NULL,
  `цена` float NOT NULL,
  `картинка` varchar(255) DEFAULT NULL,
  `цвет` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `страна` varchar(255) NOT NULL,
  `способ_применения` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `состав` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `объём` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `товар`
--

INSERT INTO `товар` (`id`, `наименование`, `id_категории`, `вес`, `цена`, `картинка`, `цвет`, `страна`, `способ_применения`, `состав`, `объём`) VALUES
(1, 'Vivienne Sabo Гель для бровей и ресниц Fixateur', 1, 10, 196, 'https://cdn1.ozone.ru/s3/multimedia-x/6036152853.jpg', 'прозрачный', 'Россия', 'Нанести щеточкой гель, расчесать и уложить. Подождать до полного высыхания.', 'Water (Aqua), PVP, Triethanolamine, Carbomer, Mica, Phenoxyethanol, Methylparaben, Butylparaben, Ethylparaben, Propylparaben, CI 77499, Triethoxycaprylylsilane , CI 77491, CI 77499, Parfum, Hydroxyisohexyl3-Cyclohexenecarboxaldehyde, Disodium EDTA.', '6 мл'),
(2, 'ENOUGH Увлажняющий тональный крем с коллагеном Collagen Moisture Foundation SPF15', 1, 147, 433, 'https://cdn1.ozone.ru/s3/multimedia-0/c1200/6011923524.jpg', 'бежевый', 'Южная Корея', 'Нанесите необходимое количество тонального средства и равномерно растушуйте, используя специальный спонж, кисть или подушечки пальцев. ', 'диоксид титана, бутиленгликоль, сорбитан сесквикаприлат, горца многоцветкового экстракт, каприловые каприновые глицериды, коллаген гидролизованный, триглицерид, диметикон, вода очищенная, циклопентасилоксан, гиалуроновая кислота, глицерин', '100 мл'),
(3, 'L\'Oreal Paris Тушь для ресниц \"Телескопик\", для удлинения и разделения', 1, 25, 486, 'https://cdn1.ozone.ru/s3/multimedia-8/wc1200/6040185188.jpg', 'экстрачёрный', 'Франция', 'Нанесите тушь на ресницы мягкими движениями по направлению роста ресниц и вытягивая их, тем самым обеспечивая максимальную длину. Для достижения более выраженного эффекта невероятно длинных ресниц нанесите тушь в 2 слоя.', 'Aqua/water, paraffin, cera alba/beeswax, stearic acid, cera carnauba/carnauba wax, acacia senegal/acacia senegal gum, palmitic acid, triethanolamine, hydroxyethylcellulose, aminomethyl propanediol, peg-40 stearate, sodium polymethacrylate, methylparaben, propylparaben, hydrogenated jojoba oil, hydro-genated palm oil, simethicone, bht, polyquaternium-10, panthenol [+/- may contain: ci 77492, ci 77499, ci 77491/iron oxides, ci 77266/black 2, ci 77007/ultramarines, ci 77288/chromium oxide greens, ci 77289/chromium hydroxide green, mica, ci 77891/titanium dioxide, ci 75470/carmine, ci 77510/ferric ferrocyanide].', '8 мл'),
(4, 'Maybelline New York Гель-лайнер для глаз \"Lasting Drama\", стойкий', 1, 3, 547, 'https://cdn1.ozone.ru/s3/multimedia-c/wc1200/6040167912.jpg', 'чёрный', 'Германия', 'Чтобы создать эффектный драматический образ, возьми на кисточку небольшое количество подводки и проведи по верхнему веку линию, расширяя ее к внешнему уголку глаза.', 'Trimethylsiloxysilicate, cyclopentasiloxane, isododecane, cyclohexasiloxane, ceresin, caprylyl methicone, synthetic fluorphlogopite, glass, silica silylate, talc, disteardimonium hectorite, calcium aluminum borosilicate, silica, triethoxycaprylylsilane, propylene carbonate, tin oxide, bht, aloe barbadensis extract/aloe barbadensis leaf extract, [+/- may contain: ci 77491, ci 77492, ci 77499/iron oxides, ci 77891/titanium dioxide, mica, ci 77007/ultramarines, ci 75470/carmine, ci 77510/ferric ferrocyanide, ci 42090/blue 1 lake].', '3 мл'),
(5, 'Elizavecca Сыворотка для лица с гиалуроновой кислотой 97% Hell Pore Control Hyaluronic Acid', 2, 125, 599, 'https://cdn1.ozone.ru/s3/multimedia-8/6004776668.jpg', 'прозрачный', 'Корея', 'Сыворотку можно использовать как самостоятельно, так и добавлять в ваш уходовый тонер или крем, а также применять в паре с альгинатной маской и мезороллером. Нанесите несколько капель сыворотки на очищенную и тонизированную кожу лица, дайте впитаться. Увлажняет, осветляет, питает и тонизирует кожу лица. Также сыворотка обладает антивозрастным уходом.Подходит для всех типов кожи.', 'Sodium Hyaluronate (97%), Niacinamide, 1,2-Hexanediol, Propylene Glycol, PEG-60 Hydrogenated Castor Oil, Carbomer, Arginine, Adenosine, Allantoin, Fragrance.', '50 мл'),
(6, 'ARAVIA Laboratories Пенка для умывания с муцином улитки и гинкго билоба Energy Skin Foam', 2, 225, 540, 'https://cdn1.ozone.ru/s3/multimedia-r/c1200/6015192711.jpg', 'прозрачный', 'Россия', 'Небольшое количество пенки нанесите на влажную кожу лица, шеи и декольте. Очистите кожу легкими массажными движениями. Смойте водой. Рекомендуется для ежедневного использования утром и вечером. Подходит для всех типов кожи.', 'Aqua, Sodium Laureth Sulfate, Glycerin, Disodium Laureth Sulphosuccinate Cocamidopropyl Betaine, Benzyl Alcohol (and) Ethylgexylglycerin, Cocamide DEA, PEG-40 Hydrogenated Castor Oil, Glycyrrhiza Glabra (licorice) Extract, Snail Secretion Filtrate, Ginkgo Biloba Leaf Extract, Parfum, Methylisotiazolinon, Disodium EDTA. ', '150 мл'),
(7, 'CeraVe Лосьон увлажняющий, для сухой и очень сухой кожи лица и тела', 2, 345, 685, 'https://cdn1.ozone.ru/s3/multimedia-d/c1200/6009800725.jpg', 'белый', 'Франция', 'Нанесите необходимое количество крема на кожу и распределите массирующими движениями. Избегайте области вокруг глаз.', 'AQUA/WATER, GLYCERIN, CAPRYLIC/CAPRIC TRIGLYCERIDE, CETEARYL ALCOHOL, CETYL ALCOHOL, DIMETHICONE, PHENOXYETHANOL, POLYSORBATE 20, CETEARETH-20, BEHENTRIMONIUM METHOSULFATE, POLYGLYCERYL-3 DIISOSTEARATE, SODIUM LAUROYL LACTYLATE, ETHYLHEXYLGLYCERIN, POTASSIUM PHOSPHATE, DISODIUM EDTA, DIPOTASSIUM PHOSPHATE, CERAMIDE NP, CERAMIDE AP, PHYTOSPHINGOSINE, CHOLESTEROL, XANTHAN GUM, CARBOMER, SODIUM HYALURONATE, TOCOPHEROL, CERAMIDE EOP', '236 мл'),
(8, 'BIOAQUA Очищающая кислородная пузырьковая маска для лица на основе глины', 2, 170, 380, 'https://cdn1.ozone.ru/multimedia/c1200/1036167617.jpg', 'зелёный', 'Китай', 'Маску рекомендуется наносить на влажную кожу. Используйте лопатку, чтобы зачерпывать средство из баночки, после наносите его на лицо и распределяйте по коже пальцами. Оставьте маску на 10-20 минут после нанесения, затем смойте теплой водой, используя спонж. После можно использовать увлажняющий крем или другое средство по уходу за кожей. Использовать маску следует 1-2 раза в неделю в зависимости от особенностей и типа кожи.', 'Water, glycerin, propylene glycol, kaolin clay, cetystearyl alcohol, hydrolyzed placental protein, jojoba oil, oats sowing (AVENA SATIN), red resin of myrrh comorfory, Wamgin\'s Witch hazel extract, potassium cetyl phosphate, garden porcelain extract, carbomer, triethanol RN, propenic acid , Acryl demitol taurate copolymer, methylisothiazoline, sodium hyaluronate, EDTA-disodium, flavor.', '100 гр'),
(9, 'Garnier Вода мицеллярная 3в1, для очищения лица, для всех типов кожи', 2, 400, 268, 'https://cdn1.ozone.ru/s3/multimedia-m/c1200/6033923650.jpg', 'прозрачный', 'Польша', 'Смочите ватный диск водой и бережно без лишнего трения, очистите кожу лица, глаза и губы. Средство не требует смывания.', 'Aqua/water, hexylene glycol, glycerin, disodium cocoamphodiacetate, disodium edta, poloxamer 184, polyaminopropyl biguanide.', '400 мл'),
(10, 'HAIRSHOP Канекалон АИДА 301 200г/130см', 3, 230, 499, 'https://cdn1.ozone.ru/s3/multimedia-2/wc1200/6034483898.jpg', 'розовый', 'Россия', NULL, NULL, NULL),
(11, 'OLLIN PROFESSIONAL PERFECT HAIR Крем-спрей многофункциональный 15 в 1 несмываемый', 3, 285, 413, 'https://cdn1.ozone.ru/multimedia/wc1200/1035616098.jpg', 'белый', 'Россия', 'Нанести на вымытые и подсушенные полотенцем волосы в необходимом количестве (в зависимости от их длины, состояния и пористости). Распределить продукт расческой по всей длине, включая кончики. Не смывая, приступить к укладке волос.', 'вода, цетеариловый спирт, дикаприлиловый эфир,бегентримония хлорид, бегениловый спирт, олеиловый эрукат, децилтетрадеканолцетиловый спирт, EDTA, глицерин, витаминный комплекс, гермаль, колаген, ши-лайт, кератин, масло карите, гиалуроновая кислота, цетеариловый дипальмитоилгидроксиэтиаммония метасульфат, стеарамидопропилдиметиламин, цетеарет-20, феноксиэтанол, поликватерниум-7, лимонная кислота', '250 мл'),
(12, 'Compliment Naturalis Маска для волос 3в1 с перцем', 3, 538, 244, 'https://cdn1.ozone.ru/s3/multimedia-2/c1200/6010481870.jpg', NULL, 'Россия', 'Нанести на влажные волосы и кожу головы. Маска начинает действовать уже с 1 минуты применения. Наибольший эффект достигается к десятой минуте. Смывать теплой водой. Применять 2-3 раза в неделю в течении 1-2 месяцев. ВНИМАНИЕ! Не использовать маску на поврежденную кожу, избегать попадания в глаза и на слизистые оболочки. При попадании в глаза промыть большим количеством холодной воды. Рекомендуется перед использованием провести тест, нанеся маску на кожу за ухом или на сгиб локтя.', 'Aqua, Cetearyl Alcohol, Cetrimonium Chloride, Hydrolyzed Keratin, D-Panthenol, Sinapis Alba Seed Oil, Vanilly Butyl Ether, Perfume, Disodium EDTA, Benzyl Alcohol, Phenoxyethanol, Citric Acid', '500 мл'),
(13, 'Constant Delight эликсир для волос многофункциональный \"12 в 1\"', 3, 200, 497, 'https://cdn1.ozone.ru/s3/multimedia-x/wc1200/6046161105.jpg', NULL, 'Италия', 'Встряхнуть флакон перед использованием. Нанести продукт на вымытые и подсушенные полотенцем волосы в необходимом количестве, в зависимости от длины, состояния и пористости волос. Распределить продукт с помощью расчески по всей длине волос, включая кончики. После нанесения продукт не смывать, приступить к укладке волос.', 'Aqua (Water), Myristyl alcohol, Quaternium-80, Cyclopentasiloxane, Cetrimonium chloride, Amodimethicone, Imidazolidinyl urea, Dimethiconol, Argania spinosa kernel oil, Linum usitatissinum (Linseed) seed oil, Macadamia integrifolia seed oil, Sodium laneth-40 maleate/styrene sulfonate copolymer, Propylene glycol, Trideceth-10, Hydrolyzed wheat protein, Butylene glycol, Hydroxypropyltrimonium hydrolyzed corn starch, Benzophenone-4, Citric acid, Keratin amino acids, Hydrolyzed silk, Hypnea musciformis extract, Methylchloroisothiazolinone, Methylisothiazolinone, CI 42051, CI 19140, Parfum (Fragrance).', '200 мл'),
(14, 'Estel Professional Curex Classiс Основной уход Шампунь для всех типов волос', 3, 1050, 500, 'https://cdn1.ozone.ru/multimedia/c1200/1021069288.jpg', NULL, 'Россия', NULL, 'Aqua, Sodium Laureth-5 Carboxylate, Disodium Laureth Sulfosuccinate, Cocamidopropul Betaine, PEG-7 Glyceryl Cocoate, PEG-200 Hydrogenated Glyceryl Palmate, Betaine, Polyquaternium-10, PEG12 Dimethicone, Panthenol, Glycine, Glycerin, PEG-400, Mannitol, Tromethamine, Glutamic Acid, Arginine HCI, Alanine, Aspartic Acid, Lysine Hydrochloride, Leucine, Valine, Citric Acid, Isopropyl Alcohol, Sodium Lactate, Sorbitol, Glucose, Phenylalanine, Isoleucine, Tyrosine, Histidine Hydrochloride, Soy Protein, Copper Tripeptide-1, Parfum, Linalool, Limonene, Hexyl Cinnamal, Citronelool, Magnesium Ascorbyl Phosphate, Methylchloroisothiazolinone, Methylisothiazolinone', '1000 мл'),
(15, 'Набор для окрашивания волос Помогалочка 4 предмета(емкость + 2 кисти для окрашивания волос, расческа)', 3, 230, 299, 'https://cdn1.ozone.ru/s3/multimedia-d/c1200/6021171145.jpg', 'чёрный', 'Россия', NULL, NULL, NULL),
(16, 'ARAVIA Professional Крем для рук Cream Oil с маслом кокоса и манго', 4, 54, 675, 'https://cdn1.ozone.ru/s3/multimedia-g/c1200/6014555032.jpg', 'белый', 'Россия', 'Небольшое количество крема нанесите на кожу рук, провести экспресс-массаж в течение 3-5 минут до полного впитывания.', 'Aqua, Mangifera Indica (Mango) Seed Butter, Cocos Nucifera Oil, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Theobroma Cacao (Cocoa) Seed Butter, Butyrospermum Parkii (Shea Butter), Hydrolyzed Collagen, Paraffinum Liquidum, Glycerin, Urea, Glyceryl Stearate, Ceteareth-20, Potassium Cetyl Phosphate, Dimethicone, Tocopheryl Acetate, Retinyl Palmitat, Melissa Officinalis Extract, Plantago Major Leaf Extract, Achillea Millefolium Extract, Cetyl Alcohol, Parfum, Phenoxyethanol (and) Ethylhexylglycerin. ', '550 мл'),
(17, 'Organic Shop Фрукты Пена для ванн кокосовый рай', 4, 550, 185, 'https://cdn1.ozone.ru/multimedia/c1200/1022748866.jpg', 'белый', 'Россия', 'Небольшое количество пены налить под струю воды при наполнении ванны. Не тестируется на животных, Без парабенов, Без сульфатов, Органическая косметика.', 'Aqua with infusions of Organic Cocos Nucifera Oil (органическое масло кокоса), Organic Oryza Sativa Extract (органическое рисовое молоко), Sodium Coco-Sulfate, Cocamidopropyl Betaine, Lauryl Glucoside, Glycerin, Sodium Chloride, Parfum, Styrene/Acrylates Copolymer, Citric Acid, Kathon.', '500 мл'),
(18, 'Nivea Creme Увлажняющий универсальный крем, для лица, рук и тела с пантенолом', 4, 270, 263, 'https://cdn1.ozone.ru/s3/multimedia-l/c1200/6044636061.jpg', 'белый', 'Германия', NULL, 'Aqua, Paraffinum Liquidum, Cera Microcristaltina, Glycerin, Lanolin Alcohol (Eucerit), Paraffin, Panthenol, Magnesium Sulfate, Decyl Oleate, Octyldodecanol, Aluminum Stearates, Citric Acid, Magnesium S tea rate, limonene, Geraniol, Hydroxycitronellal, Linalool, Citronellol, Benzyl Benzoate, Cinnamyl Alcohol, Parfum', '250 мл'),
(19, 'Holika Holika Гель для душа с алоэ вера Aloe 92% Shower Gel', 4, 310, 270, 'https://cdn1.ozone.ru/s3/multimedia-y/wc1200/6046900078.jpg', 'зелёный', 'Южная Корея', NULL, NULL, '250 мл'),
(20, 'EXOTIC COSMETICS Кофейный скраб для тела с нежным ароматом кокоса', 4, 225, 219, 'https://cdn1.ozone.ru/s3/multimedia-n/c1200/6041627795.jpg', 'чёрный', 'Россия', 'Нанести на влажную кожу лёгкими массирующими движениями. Смыть скраб водой.', 'Кофейные зерна, гималайская соль, масло жожоба, масло макадамии, эфирное масло сицилийского лимона, миндальное масло, масло кокоса.', '220 гр'),
(21, 'Lacoste Pour Femme Парфюмерная вода', 5, 150, 2153, 'https://cdn1.ozone.ru/s3/multimedia-h/wc1200/6015775289.jpg', NULL, 'Испания', NULL, 'Alcohol Denat., Aqua (Water), Parfum (Fragrance), Ethylhexyl Methoxycinnamate, Diethylamino Hydroxybenzoyl Hexyl Benzoate, BHT, Linalool, Alpha Isomethyl Ionone, Hydroxyisohexyl 3 Cyclohexene Carboxaldehyde, Geraniol, Farnesol, Citronellol, CI 17200 (Red 33), CI 19140 (Yellow 5).', '30 мл'),
(22, 'City Parfum Rainbow Душистая вода', 5, 120, 245, 'https://cdn1.ozone.ru/multimedia/c1200/1025216462.jpg', 'розовый', 'Россия', NULL, 'AQUA (WATER), ALCOHOL DENAT., PARFUM (FRAGRANCE).', '30 мл'),
(23, 'Набор автопарфюмов для мужчин, парфюм в авто', 5, 590, 2925, 'https://cdn1.ozone.ru/s3/multimedia-y/c1200/6028567834.jpg', NULL, 'Россия', NULL, NULL, '12 мл * 10'),
(24, 'RuNail Professional Гель для наращивания камуфлирующий \"Ягодное мороженое\", 3452,15 г', 6, 30, 395, 'https://cdn1.ozone.ru/s3/multimedia-k/c1200/6017977700.jpg', 'тёмно-розовый', 'Россия', NULL, 'Олигомер полиуретанакрилата, гидроксиэтил метакрилат, пигмент.', '15 гр'),
(25, 'Start Epil Крем-парафин \"Фруктовый щербет\", 150 мл', 6, 220, 259, 'https://cdn1.ozone.ru/s3/multimedia-5/c1200/6019458977.jpg', 'оранжевый', 'Россия', 'Интенсивными растирающими движениями разогреть крем-парафин в ладонях и распределить по коже кистей рук и стоп. Накрыть защитной пленкой и теплоудерживающим материалом. Через 15-20 минут снять остатки крем-парафина салфеткой. Не смывать водой. ', 'Paraffinum Liquidum, Paraffin, Lanolin, Butyrospermum Parkii (Shea Butter), Mangifera Indica (Mango) Seed Butter, Citrus aurantium fruit extract, Tocopheryl Acetate, CI47000, CI26100, perfume.', '150 мл'),
(26, 'Масло для кутикулы и ногтей Solomeya \"Сладкий миндаль\", с витаминами', 6, 60, 235, 'https://cdn1.ozone.ru/s3/multimedia-w/c1200/6000125780.jpg', 'оранжевый', 'Великобритания', 'Нанести кисточкой на зону кутикулы и матрикса и деликатно втереть массажными движениями.', 'Glycine Soya Oil,Olea Europaea Fruit Oil,Parfum,Simmondsia Chinensis (Jojoba) Oil,Amarantus Cuadatus Seed Oil,Nigella Sativa Seed Oil,Linoleic / Linolic Acid ,Tocoferol ,Retynul Palmitate ,BHT.May contais CI61565,CI60725,CI 26100,Carotene.', '14 мл'),
(27, 'Лошадиная сила Мега укрепитель для поврежденных ногтей с алмазной крошкой, кальцием, витаминами А и В5', 6, 215, 340, 'https://cdn1.ozone.ru/s3/multimedia-r/wc1200/6021478851.jpg', NULL, 'Россия', 'Нанести 1-2 слоя на сухие чистые ногти один раз в день, время суток не влияет на эффект от применения. Может использоваться как основу под лак. Дополнительными слоями средства покрывают в течение 3-5 дней, при этом предыдущие слои не удаляют. Затем средство полностью удаляют с ногтей, в том числе используя стандартную жидкость для снятия лака. Последующий день ногтевая пластина отдыхает от воздействия активных веществ в составе средства. Далее процедура полностью повторяется до достижения видимого эффекта. Период использования не ограничен.', 'Butyl Acetate, Ethyl Acetate, Nitrocellulose, Adipic Acid/Neopentyl Glycol/Trimellitic Anhydride Copolymer, Acetyl Tributyl Citrate, Isopropyl Alcohol, Acrylates Copolymer, Stearalkonium Hectorite, Benzophenone - 1, Benzophenone – 2, Dimond Powder, Calcium Pantothenate, Retinyl Palmitate, Pantothenic Acid, CI 60725.', '17 мл'),
(28, 'VILENTA Маска-носочки для ног Увлажняющая SILKY FOOT с маслами Ши, Жожоба, Макадамии и Арганы', 6, 45, 214, 'https://cdn1.ozone.ru/s3/multimedia-o/wc1200/6006697032.jpg', NULL, 'Россия', '1. Откройте упаковку. 2. Вымойте ноги в теплой водой. 3. Наденьте маску на ноги и плотно зафиксируйте ее. 4. Через 20-30 минут снимите маску, а остатки маски равномерно распределите по коже ног. 5. Для наибольшего эффекта от Отшелушивающей маски-носочков VILENTA Shiny Foot рекомендуется после ее применения регулярно использовать Увлажняющую маску-носочки для ног VILENTA Silky Foot.', 'Water,Glycerin,Urea,Glyceryl stearate,Peg-40,Stearate,Dimethicone,Caprylic/Caorictriglyceride,Cetearyl alcohol,Butyrospermum parkii (Shea butter),Focus visiculosus extract,Trehalose,Potassium cetyl phosphate,Macadamia ternifolia seed oil,Chamomilla recutita (matricaria) flower extract,Calendula officinalis (marigold) flower extract,Acrylates/c10-30 alkyl acrylate crosspolymer,xanthan gum,Allantoin,Prunus amigdalus dulcis (sweet almond) oil,Simmonadsia chinensis (jojoba) seed oil,Citric acid,Panthenol,Glyceryl caprylate,Glyceryl caprate,Disodium edta,Tocopheryl acetate,Bht,Sodium hydroxideMenthol,Phenoxyethanol,Parfum.', '40 гр'),
(29, 'Отбеливающий комплекс IVISMILE DUAL / Беспроводная система для отбеливания зубов / результат 8 тонов за 5 дней / в наборе: Led капа, отбеливающий гель-карандаш 3 шт., инструкция, шкала оттенков', 7, 335, 2665, 'https://cdn1.ozone.ru/s3/multimedia-c/wc1200/6025871124.jpg', NULL, 'Китай', '1. Почистите зубы, прополощите рот водой; 2. Удалите излишнюю влагу с наружной поверхности зубов; Откройте и поверните карандаш по часовой стрелке до появления геля на аппликаторе; 3. Тонким и равномерным слоем нанесите отбеливающий гель на наружную поверхность зубов в зоне улыбки. Один карандаш рассчитан на 1-2 сеанса отбеливания; 4. Одним нажатием включите Led капу и поместите её в рот; 5. Через 15 мин. Led капа отключится, вытащите (промойте её) и прополощите рот водой; 6. Повторяйте сеанс отбеливания один раз в день по 15 мин в течении 3-6 дней с момента начала курса. Соблюдайте рекомендации по белой диете.', 'Carbamide Peroxide, Glycerol, Propelyne Glycol, Deionized Water, Carboxymethyl, Polyvinylpyrrolidone, Sodium Hydroxide, Menthol', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `товар_заказа`
--

CREATE TABLE `товар_заказа` (
  `id` int NOT NULL,
  `id_заказа` int NOT NULL,
  `id_товара` int NOT NULL,
  `кол-во` int NOT NULL,
  `цена, ₽` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `товар_заказа`
--

INSERT INTO `товар_заказа` (`id`, `id_заказа`, `id_товара`, `кол-во`, `цена, ₽`) VALUES
(1, 1, 2, 1, 433),
(2, 2, 9, 2, 536),
(3, 3, 23, 1, 2925),
(4, 4, 17, 3, 555),
(5, 5, 24, 1, 395),
(6, 6, 15, 1, 299),
(7, 7, 13, 1, 497),
(8, 8, 22, 2, 490),
(9, 9, 1, 2, 392),
(10, 10, 10, 1, 499),
(11, 10, 5, 1, 599);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Индексы таблицы `заказ`
--
ALTER TABLE `заказ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_покупателя` (`id_покупателя`);

--
-- Индексы таблицы `категория_товара`
--
ALTER TABLE `категория_товара`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `оплата`
--
ALTER TABLE `оплата`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_покупателя` (`id_покупателя`);

--
-- Индексы таблицы `покупатель`
--
ALTER TABLE `покупатель`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `товар`
--
ALTER TABLE `товар`
  ADD PRIMARY KEY (`id`),
  ADD KEY `товар_ibfk_1` (`id_категории`);

--
-- Индексы таблицы `товар_заказа`
--
ALTER TABLE `товар_заказа`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_заказа` (`id_заказа`),
  ADD KEY `id_товара` (`id_товара`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `заказ`
--
ALTER TABLE `заказ`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `категория_товара`
--
ALTER TABLE `категория_товара`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `оплата`
--
ALTER TABLE `оплата`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `покупатель`
--
ALTER TABLE `покупатель`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `товар`
--
ALTER TABLE `товар`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `товар_заказа`
--
ALTER TABLE `товар_заказа`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `заказ`
--
ALTER TABLE `заказ`
  ADD CONSTRAINT `заказ_ibfk_1` FOREIGN KEY (`id_покупателя`) REFERENCES `покупатель` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `оплата`
--
ALTER TABLE `оплата`
  ADD CONSTRAINT `оплата_ibfk_1` FOREIGN KEY (`id_покупателя`) REFERENCES `покупатель` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `товар`
--
ALTER TABLE `товар`
  ADD CONSTRAINT `товар_ibfk_1` FOREIGN KEY (`id_категории`) REFERENCES `категория_товара` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `товар_заказа`
--
ALTER TABLE `товар_заказа`
  ADD CONSTRAINT `товар_заказа_ibfk_1` FOREIGN KEY (`id_заказа`) REFERENCES `заказ` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `товар_заказа_ibfk_2` FOREIGN KEY (`id_товара`) REFERENCES `товар` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
