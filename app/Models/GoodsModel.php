<?php namespace App\Models;

use CodeIgniter\Model;



class GoodsModel extends Model
{
    protected $table = 'goods'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['id', 'наименование', 'id_категории', 'вес', 'цена', 'картинка', 'цвет', 'страна', 'способ_применения', 'состав', 'объём', 'user_id', 'picture_url'];

    public function getGoods($id = null)
    {
        $builder = $this->select('*')->join('category_goods', 'goods.id_категории = category_goods.id_category');
        if (!isset($id)) {
            return $builder;
        }
        return $builder->where(['id' => $id])->first();
    }

    public function getGoodsWithUser($id = null, $search = '')
    {
        //$builder = $this->select('*')->join('users', 'goods.user_id = users.id')->like('goods.наименование', $search, 'both', null, true);
        $builder = $this->select('*')->join('category_goods', 'goods.id_категории = category_goods.id_category');
        if (!is_null($id)) {
            return $builder->where(['goods.id' => $id])->first();
        }
        return $builder;
    }


}
