<?php namespace App\Controllers;

use App\Models\GoodsModel;
use Aws\S3\S3Client;

class Goods extends BaseController
{

    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoodsModel();
        //значение меняется здесь
        $data['goods'] = $model->getGoods()->paginate(9, 'group1');
        $data['pager'] = $model->pager;

        echo view('goods/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        //if (!$this->ionAuth->loggedIn())
        //{
        //    return redirect()->to('/auth/login');
        //}
        $model = new GoodsModel();
        $data ['goods'] = $model->getGoods($id);
        echo view('goods/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('goods/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'наименование' => 'required',
                'id_категории'  => 'required',
                'вес'  => 'required',
                'цена'  => 'required',
                'цвет'  => 'required',
                'картинка'  => 'required',
                'страна' => 'required',
                'способ_применения'  => 'required',
                'состав'  => 'required',
                'объём'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new GoodsModel();
            $data = [
                'наименование' => $this->request->getPost('наименование'),
                'id_категории' => $this->request->getPost('id_категории'),
                'вес' => $this->request->getPost('вес'),
                'цена' => $this->request->getPost('цена'),
                'картинка' => $this->request->getPost('картинка'),
                'цвет' => $this->request->getPost('цвет'),
                'страна' => $this->request->getPost('страна'),
                'способ_применения' => $this->request->getPost('способ_применения'),
                'состав' => $this->request->getPost('состав'),
                'объём' => $this->request->getPost('объём'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Товар "'.$this->request->getPost('наименование').'" успешно добавлен'));
            return redirect()->to('/goods');
        }
        else
        {
            return redirect()->to('/goods/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoodsModel();

        helper(['form']);
        $data ['goods'] = $model->getGoods($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('goods/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/goods/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'наименование' => 'required',
                'id_категории'  => 'required',
                'вес'  => 'required',
                'цена'  => 'required',
                'цвет'  => 'required',
                'картинка'  => 'required',
                'страна' => 'required',
                'способ_применения'  => 'required',
                'состав'  => 'required',
                'объём'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new GoodsModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'наименование' => $this->request->getPost('наименование'),
                'id_категории' => $this->request->getPost('id_категории'),
                'вес' => $this->request->getPost('вес'),
                'цена' => $this->request->getPost('цена'),
                'картинка' => $this->request->getPost('картинка'),
                'цвет' => $this->request->getPost('цвет'),
                'страна' => $this->request->getPost('страна'),
                'способ_применения' => $this->request->getPost('способ_применения'),
                'состав' => $this->request->getPost('состав'),
                'объём' => $this->request->getPost('объём'),
            ];
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            return redirect()->to('/goods/view/'.$this->request->getPost('id'));
        }
        else
        {
           return redirect()->to('/goods/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new GoodsModel();
        $model->delete($id);
        return redirect()->to('/goods');
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new GoodsModel();
            $data['goods'] = $model->getGoodsWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('goods/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('OnlineStore.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }
}
