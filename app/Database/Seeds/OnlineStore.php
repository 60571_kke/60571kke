<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class OnlineStore extends Seeder
{
    public function run()
    {
        $data = [
            'ФИО'=> 'Савина А.Н.',
            'адрес_доставки'=>'ул. Пушкина д.17 кв.99',
            'e-mail' => 'savina_an@pochta.ru'
        ];
        $this->db->table('покупатель')->insert($data);


        $data = [
            'id_покупателя' => 1,
            'дата_и_время_отправки' => '2021-02-25 12:57:49',
        ];
        $this->db->table('заказ')->insert($data);


        $data = [
            'name_category' => 'Макияж',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Уход за лицом',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Уход за волосами',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Уход за телом',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Парфюмерия',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Маникюр и педикюр',
        ];
        $this->db->table('category_goods')->insert($data);

        $data = [
            'name_category' => 'Гигиена полости рта',
        ];
        $this->db->table('category_goods')->insert($data);


        $data = [
            'id_покупателя'=> 1,
            'дата_и_время_отправки'=>'2021-02-25 09:03:44',
            'сумма' => 433
        ];
        $this->db->table('оплата')->insert($data);


        $data = [
            'наименование'=> 'Vivienne Sabo Гель для бровей и ресниц Fixateur',
            'id_категории'=> 1,
            'вес' => 10,
            'цена'=> 196,
            'картинка'=>'https://cdn1.ozone.ru/s3/multimedia-x/6036152853.jpg',
            'цвет' => 'прозрачный',
            'страна'=> 'Россия',
            'способ_применения'=>'Нанести щеточкой гель, расчесать и уложить. Подождать до полного высыхания.',
            'состав' => 'Water (Aqua), PVP, Triethanolamine, Carbomer, Mica, Phenoxyethanol, Methylparaben, Butylparaben, Ethylparaben, Propylparaben, CI 77499, Triethoxycaprylylsilane , CI 77491, CI 77499, Parfum, Hydroxyisohexyl3-Cyclohexenecarboxaldehyde, Disodium EDTA.',
            'объём'=> '6 мл',
        ];
        $this->db->table('goods')->insert($data);

        $data = [
            'наименование'=> 'ENOUGH Увлажняющий тональный крем с коллагеном Collagen Moisture Foundation SPF15',
            'id_категории'=> 1,
            'вес' => 147,
            'цена'=> 433,
            'картинка'=>'https://cdn1.ozone.ru/s3/multimedia-0/c1200/6011923524.jpg',
            'цвет' => 'бежевый',
            'страна'=> 'Южная Корея',
            'способ_применения'=>'Нанесите необходимое количество тонального средства и равномерно растушуйте, используя специальный спонж, кисть или подушечки пальцев. ',
            'состав' => 'диоксид титана, бутиленгликоль, сорбитан сесквикаприлат, горца многоцветкового экстракт, каприловые каприновые глицериды, коллаген гидролизованный, триглицерид, диметикон, вода очищенная, циклопентасилоксан, гиалуроновая кислота, глицерин',
            'объём'=> '100 мл',
        ];
        $this->db->table('goods')->insert($data);


        $data = [
            'id_заказа'=> 1,
            'id_товара' => 2,
            'кол-во' => 1,
            'цена' => 433
        ];
        $this->db->table('товар_заказа')->insert($data);

    }
}
