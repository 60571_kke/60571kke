<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OnlineStore extends Migration
{
    public function up()
    {
        //покупатель
        if (!$this->db->tableexists('покупатель'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ФИО' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'адрес_доставки' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'e-mail' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('покупатель', TRUE);
        }

        // заказ
        if (!$this->db->tableexists('заказ'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_покупателя' => array('type' => 'INT', 'null' => FALSE),
                'дата_и_время_отправки' => array('type' => 'DATETIME', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_покупателя','покупатель','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('заказ', TRUE);
        }

        //категория товара
        if (!$this->db->tableexists('category_goods'))
        {
            // Setup Keys
            $this->forge->addkey('id_category', TRUE);

            $this->forge->addfield(array(
                'id_category' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name_category' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('category_goods', TRUE);
        }

        //оплата
        if (!$this->db->tableexists('оплата'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_покупателя' => array('type' => 'INT', 'null' => FALSE),
                'дата_и_время_отправки' => array('type' => 'DATETIME', 'null' => FALSE),
                'сумма' => array('type' => 'FLOAT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_покупателя','покупатель','id','RESTRICT','RESTRICT');;
            // create table
            $this->forge->createtable('оплата', TRUE);
        }

        //товар
        if (!$this->db->tableexists('goods'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'наименование' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'id_категории' => array('type' => 'INT', 'null' => FALSE),
                'вес' => array('type' => 'FLOAT', 'null' => FALSE),
                'цена' => array('type' => 'FLOAT', 'null' => FALSE),
                'картинка' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'цвет' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'страна' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'способ_применения' => array('type' => 'TEXT', 'null' => TRUE),
                'состав' => array('type' => 'TEXT', 'null' => TRUE),
                'объём' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
            $this->forge->addForeignKey('id_категории','category_goods','id_category','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('goods', TRUE);
        }

        //товар_заказа
        if (!$this->db->tableexists('товар_заказа'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'id_заказа' => array('type' => 'INT', 'null' => FALSE),
                'id_товара' => array('type' => 'INT', 'null' => FALSE),
                'кол-во' => array('type' => 'INT', 'null' => FALSE),
                'цена' => array('type' => 'FLOAT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_заказа','заказ','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_товара','goods','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('товар_заказа', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('заказ');
        $this->forge->droptable('category_goods');
        $this->forge->droptable('оплата');
        $this->forge->droptable('покупатель');
        $this->forge->droptable('goods');
        $this->forge->droptable('товар_заказа');
    }
}
