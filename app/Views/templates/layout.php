<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Интернет-магазин PERFECT</title>
    <link rel="stylesheet" type="text/css" href="/style.css"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"/>

    <link href="https://fonts.googleapis.com/css2?family=Codystar&family=Open+Sans&family=Raleway&display=swap"
          rel="stylesheet"/>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@600&display=swap" rel="stylesheet">

    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script>
    <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body>

<div class="fixed-top d-flex flex-column flex-lg-row">
    <!--правая часть навбара-->
    <nav class="navbar w-100 px-2 px-lg-5 navbar-expand-lg navbar-dark bg-dark text-white">
        <!--лого-->
        <a class="navbar-brand mr-4" href="<?= base_url() ?>"><img src="/img/logo_dark.png" height="40"
                                                                   alt="logo_navbar"></a>

        <!--кнопка при сворачивании меню-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <!--li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active py-lg-2 py-3 text-center" href="#" id="dropdown01"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true">Товары
                        <span class="iconify" data-icon="eva:arrow-ios-downward-fill" data-inline="false"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="<!?= base_url() ?>/index.php/goods">Все товары</a>
                        <!?php if ($ionAuth->isAdmin()): ?>
                            <a class="dropdown-item" href="<!?= base_url() ?>/goods/store">Добавить товар</a>
                        <!?php endif ?>
                    </div>
                </li-->

                <li class="nav-item active d-flex flex-row justify-content-end">
                    <a class="nav-link p-2" href="<?= base_url() ?>/index.php/goods">Товары
                        <?php if ($ionAuth->isAdmin() && ($ionAuth->user()->row()->id == 1)): ?>
                        <a class="text-white d-flex align-items-center" href="<?= base_url() ?>/goods/store">
                            <span class="iconify" data-icon="akar-icons:circle-plus" data-inline="false" style="height: 25px; width: 25px;"></span>
                        </a>
                        <?php endif ?>
                    </a>
                </li>

                <!--li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="true">Мой профиль</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="#">Мой профиль</a>
                    </div>
                </li-->

                <?php if ($ionAuth->isAdmin() && ($ionAuth->user()->row()->id == 1)): ?>
                    <li class="nav-item active ml-lg-3 d-flex justify-content-end">
                        <a class="nav-link p-2 d-flex flex-row" href="<?= base_url() ?>/goods/viewAllWithUsers">Админ
                            <span class="iconify ml-2" data-icon="wpf:administrator" data-inline="false" style="height: 25px; width: 25px;"></span>
                        </a>
                    </li>
                <?php endif ?>

            </ul>
        </div>

    </nav>

    <!--левая часть навбара-->
    <div class="navbar_icons d-flex align-items-center justify-content-end w-100 bg-dark">
        <!--поиск-->
        <form class="navbar_search text-white p-2 p-lg-4">
            <input type="search" id="navbar_search" class="bg-dark" placeholder="Поиск" />
            <span class="iconify navbar_iconify__search" data-icon="gg:search" data-inline="false"></span>
        </form>

        <!--корзина-->
        <div class="navbar_basket p-2 p-md-3 p-lg-4">
            <a class="text-white" href="#">
                <span class="iconify navbar_iconify__basket" data-icon="clarity:shopping-bag-line"
                      data-inline="false"></span>
            </a>
        </div>


        <?php if (!$ionAuth->loggedIn()): ?>
            <!--div class="nav-item dropdown">
                    <a class="nav-link active" href="<?= base_url() ?>/auth/login"><span
                                class="fas fa fa-sign-in-alt"></span>&nbsp;&nbsp;Вход
                    </a>
                </div-->

            <div class="dropdown navbar_iconify p-2 p-md-3 p-lg-4">
                <a class="text-white" id="dropdown01" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span class="iconify navbar_iconify__account" data-icon="teenyicons:user-circle-outline"
                          data-inline="false"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown01">
                    <div class="d-flex flex-column justify-content-center align-items-center ">
                        <span class="iconify iconify_account__dropdown" data-icon="line-md:account"
                              data-inline="false"></span>
                        <div class="text_account__dropdown">Вход не выполнен</div>
                    </div>
                    <div class="d-flex flex-column flex-lg-row">
                        <a class="p-2" href="<?= base_url() ?>/auth/login">
                            <button type="button" class="btn btn-success w-100">Войти</button>
                        </a>

                        <a class="p-2" href="<?= base_url() ?>/auth/register_user">
                            <button type="button" class="btn btn-primary w-100">Зарегистрироваться</button>
                        </a>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active text-white d-flex justify-content-center align-items-center p-2 p-lg-4"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span class="iconify navbar_iconify__account" data-icon="teenyicons:user-circle-solid"
                          data-inline="false"></span>
                    <div class="dropdown-toggle__text d-none d-lg-block pl-3"><?php echo $ionAuth->user()->row()->first_name; ?></div>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item text-danger" href="<?= base_url() ?>/auth/logout">
                        <span class="fas fa fa-sign-in-alt mr-2"></span>Выход
                    </a>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>g

<main role="main">
    <?= $this->renderSection('content') ?>
</main>

<footer class="text-center">
    <img src="/img/tracery.png" class="footer_tracery" alt="tracery_footer">

    <div class="float-center">
        <img src="/img/logo_link.png" height="30" alt="logo_footer">
    </div>

    <div class="my-3">
        <a class="d-inline mr-3 text-decoration-none" href="<?php echo base_url(); ?>/index.php/pages/view/agreement">
            <span class="iconify footer_iconify__eula" data-icon="system-uicons:document-stack" data-inline="false"></span>
            EULA
        </a>
        <div class="d-inline">©60571kke</div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
</footer>

</body>

</html>
