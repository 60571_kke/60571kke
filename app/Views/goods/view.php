<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($goods)) : ?>
            <div class="card mb-3">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center justify-content-center my-2">
                        <?php if (strlen($goods['picture_url']) > 0) : ?>
                            <img class="card-img-top" height="300" alt="picture_goods" src="<?= esc($goods['picture_url']); ?>" style="object-fit: contain;">
                        <?php else:?>
                            <img class="card-img-top" height="300" alt="img_goods" src="<?= esc($goods['картинка']); ?>" style="object-fit: contain;">
                        <?php endif ?>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($goods['наименование']); ?></h5>
                            
                            <?php if ($goods['состав'] != NULL) : ?>
                                <div class="mb-2">
                                    <strong>Состав средства</strong>
                                    <div class="text-muted"><?= esc($goods['состав']); ?></div>
                                </div>
                            <?php endif ?>

                            <?php if ($goods['цвет'] != NULL) : ?>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0">Цвет</div>
                                    <div class="text-muted"><?= esc($goods['цвет']); ?></div>
                                </div>
                            <?php endif ?>

                            <?php if ($goods['объём'] != NULL) : ?>
                                <div class="d-flex justify-content-between">
                                    <div class="my-0">Объём\Вес</div>
                                    <div class="text-muted"><?= esc($goods['объём']); ?></div>
                                </div>
                            <?php endif ?>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Страна изготовитель</div>
                                <div class="text-muted"><?= esc($goods['страна']); ?></div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Вес в упаковке</div>
                                <div class="text-muted"><?= esc($goods['вес']); ?> г</div>
                            </div>

                            <?php if ($goods['способ_применения'] != NULL) : ?>
                                <div class="mt-2">
                                    <div>Способ применения: </div>
                                    <div class="text-muted"><?= esc($goods['способ_применения']); ?></div>
                                </div>
                            <?php endif ?>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Цена</div>
                                <span class="badge badge-secondary" style="font-size: 1.5em;"><?= esc($goods['цена']); ?> ₽</span>
                            </div>
                            <?php if ($ionAuth->isAdmin()): ?>
                                <div class="card-body float-md-right d-flex flex-column text-right pr-0">
                                    <a href="<?= base_url()?>/goods/edit/<?= esc($goods['id']); ?>" class="btn btn-link mt-4 text-right">Редактировать</a>
                                    <a href="<?= base_url()?>/goods/delete/<?= esc($goods['id']); ?>" class="btn btn-link text-right text-danger">Удалить</a>
                                </div>
                            <?php else: ?>
                            <?php endif ?>


                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Товар не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
