<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main">
    <?php if (!empty($goods) && is_array($goods)) : ?>

        <h2>Все товары:</h2>

        <div class="d-flex justify-content-between mb-4">

            <?= form_open('goods/viewAllWithUsers',['style' => 'display: flex; width: 50%']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-outline-info" type="submit">Найти</button>
            </form>

            <?= form_open('goods/viewAllWithUsers', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-outline-info" type="submit">На странице</button>
            </form>

        </div>

        <table class="table table-striped">
            <thead class="text-center" style="background: ivory;">
                <th scope="col">Аватар</th>
                <th scope="col">Имя</th>
                <th scope="col">Категория</th>
                <th scope="col">Цена</th>
                <th scope="col">Управление</th>
            </thead>
            <tbody>
            <?php foreach ($goods as $item): ?>
                <tr>
                    <td>
                        <?php if (strlen($item['picture_url']) > 0) : ?>
                            <img class="card-img-top" height="300" alt="picture_goods" src="<?= esc($item['picture_url']); ?>" style="object-fit: contain;">
                        <?php else:?>
                            <img class="card-img-top" height="300" alt="img_goods" src="<?= esc($item['картинка']); ?>" style="object-fit: contain;">
                        <?php endif ?>
                    </td>
                    <td><?= esc($item['наименование']); ?></td>
                    <td><?= esc($item['name_category']); ?></td>
                    <!--td><!?= esc($item['name_category']); ?></td-->
                    <td><?= esc($item['цена']); ?></td>
                    <td class="text-center">
                        <a href="<?= base_url()?>/goods/view/<?= esc($item['id']); ?>" class="btn btn-outline-primary w-75 mb-3">Просмотреть</a>
                        <a href="<?= base_url()?>/goods/edit/<?= esc($item['id']); ?>" class="btn btn-outline-success w-75 mb-3">Редактировать</a>
                        <a href="<?= base_url()?>/goods/delete/<?= esc($item['id']); ?>" class="btn btn-outline-danger w-75 mb-3">Удалить</a>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="d-flex justify-content-center my-3">
                <?= $pager->links('group1', 'my_page') ?>
        </div>

    <?php else : ?>
        <div class="text-center">
            <p>Товары не найдены </p>
            <a class="btn btn-primary btn-lg" href="<?= base_url()?>/goods/create">Добавить</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>