<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main">

    <?php if (!empty($goods) && is_array($goods)) : ?>

        <div class="d-flex justify-content-center my-3">
            <?= $pager->links('group1', 'my_page') ?>
        </div>

        <div class="row d-flex justify-content-center">

            <?php foreach ($goods as $item): ?>

                <div class="card m-3" style="width: 18rem;">
                    <div class="float-center my-3">
                        <?php if (strlen($item['picture_url']) > 0) : ?>
                            <img class="card-img-top" height="300" alt="picture_goods" src="<?= esc($item['picture_url']); ?>" style="object-fit: contain;">
                        <?php else:?>
                            <img class="card-img-top" height="300" alt="img_goods" src="<?= esc($item['картинка']); ?>" style="object-fit: contain;">
                        <?php endif ?>
                    </div>

                    <div class="card-body d-flex flex-column justify-content-between">

                        <h6 class="card-subtitle mb-3 text-muted">
                            <?= esc($item['name_category']); ?>
                        </h6>

                        <a href="<?= base_url() ?>/index.php/goods/view/<?= esc($item['id']); ?>"
                           class="card-title h5 text-dark text-decoration-none"><?= esc($item['наименование']); ?></a>

                        <div class="text-right">
                            <div>
                                <h2><?= esc($item['цена']); ?> ₽</h2>
                            </div>

                            <a href="#" class="card-body__button d-flex align-items-center justify-content-between text-decoration-none">
                                <div class="card-body__button_text text-dark ml-4"> В корзину </div>
                                <div class="card-body__button_basket py-2 px-4 text-white">
                                    <span class="iconify navbar_iconify__basket" data-icon="clarity:shopping-cart-line" data-inline="false"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="d-flex justify-content-center my-3">
            <?= $pager->links('group1', 'my_page') ?>
        </div>

    <?php else : ?>
        <p>Невозможно найти товар</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
