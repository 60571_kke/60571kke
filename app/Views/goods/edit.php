<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('goods/update'); ?>
    <input type="hidden" name="id" value="<?= $goods["id"] ?>">

    <div class="form-group">
        <label for="name">Наименование товара</label>
        <input type="text" class="form-control <?= ($validation->hasError('наименование')) ? 'is-invalid' : ''; ?>" name="наименование"
               value="<?= $goods["наименование"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('наименование') ?>
        </div>

    </div>

    <div class="form-group">
        <label class="form-check-label">Категория товара</label>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="1" <?= $goods["id_категории"]=='1' ? 'checked': '' ?> /><div class="form-text text-muted">Макияж</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="2" <?= $goods["id_категории"]=='2' ? 'checked': '' ?> /><div class="form-text text-muted">Уход за лицом</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="3" <?= $goods["id_категории"]=='3' ? 'checked': '' ?> /><div class="form-text text-muted">Уход за волосами</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="4" <?= $goods["id_категории"]=='4' ? 'checked': '' ?> /><div class="form-text text-muted">Уход за телом</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="5" <?= $goods["id_категории"]=='5' ? 'checked': '' ?> /><div class="form-text text-muted">Парфюмерия</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="6" <?= $goods["id_категории"]=='6' ? 'checked': '' ?> /><div class="form-text text-muted">Маникюр и педикюр</div>
            </label>
        </div>
        <div class="form-check ">
            <label class="form-check-label">
                <input class="form-check-input" type="radio" name="id_категории" value="7" <?= $goods["id_категории"]=='7' ? 'checked': '' ?> /><div class="form-text text-muted">Гигиена полости рта</div>
            </label>
        </div>
        <div class="invalid-feedback" style="display: block">
            <?= $validation->getError('id_категории') ?>
        </div>
    </div>


    <div class="form-group">
        <label for="name">Вес</label>
        <input type="text" class="form-control <?= ($validation->hasError('вес')) ? 'is-invalid' : ''; ?>" name="вес"
               value="<?= $goods["вес"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('вес') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Цена</label>
        <input type="text" class="form-control <?= ($validation->hasError('цена')) ? 'is-invalid' : ''; ?>" name="цена"
               value="<?= $goods["цена"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('цена') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture"
            value="<?= $goods["picture_url"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">или введите URL</label>
        <input type="text" class="form-control <?= ($validation->hasError('картинка')) ? 'is-invalid' : ''; ?>" name="картинка"
               value="<?= $goods["картинка"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('картинка') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Цвет</label>
        <input type="text" class="form-control <?= ($validation->hasError('цвет')) ? 'is-invalid' : ''; ?>" name="цвет"
               value="<?= $goods["цвет"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('цвет') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Страна</label>
        <input type="text" class="form-control <?= ($validation->hasError('страна')) ? 'is-invalid' : ''; ?>" name="страна"
               value="<?= $goods["страна"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('страна') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Способ применения</label>
        <textarea type="text" class="form-control <?= ($validation->hasError('способ_применения')) ? 'is-invalid' : ''; ?>"
                  name="способ_применения"><?= $goods["способ_применения"]; ?></textarea>
        <div class="invalid-feedback">
            <?= $validation->getError('способ_применения') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Состав</label>
        <textarea type="text" class="form-control <?= ($validation->hasError('состав')) ? 'is-invalid' : ''; ?>"
                  name="состав"><?= $goods["состав"]; ?></textarea>
        <div class="invalid-feedback">
            <?= $validation->getError('состав') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="name">Объём</label>
        <input type="text" class="form-control <?= ($validation->hasError('объём')) ? 'is-invalid' : ''; ?>" name="объём"
               value="<?= $goods["объём"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('объём') ?>
        </div>
    </div>

    <div class="form-group d-flex justify-content-center">
        <button type="submit" class="btn btn-outline-dark" name="submit">Сохранить</button>
    </div>
    </form>
</div>
<?= $this->endSection() ?>