<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('goods/store'); ?>
        <form>
            <div class="form-group">
                <label for="name" class="w-100"> Наименование товара
                    <input type="text" class="form-control mt-2 <?= /** @var $validation */
                    ($validation->hasError('наименование')) ? 'is-invalid' : ''; ?>" name="наименование"
                           value="<?= old('наименование'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('наименование') ?>
                </div>
            </div>

            <div class="form-group">
                <div class="form-check-label">Категория товара</div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="1" <?= old('id_категории') == '1' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Макияж</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="2" <?= old('id_категории') == '2' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Уход за лицом</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="3" <?= old('id_категории') == '3' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Уход за волосами</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="4" <?= old('id_категории') == '4' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Уход за телом</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="5" <?= old('id_категории') == '5' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Парфюмерия</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="6" <?= old('id_категории') == '6' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Маникюр и педикюр</div>
                    </label>
                </div>
                <div class="form-check ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="id_категории" value="7" <?= old('id_категории') == '7' ? 'checked' : '' ?> />
                        <div class="form-text text-muted">Гигиена полости рта</div>
                    </label>
                </div>
                <div class="invalid-feedback">
                    <?= $validation->getError('id_категории') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Вес
                    <input type="text" class="form-control <?= ($validation->hasError('вес')) ? 'is-invalid' : ''; ?>"
                           name="вес"
                           value="<?= old('вес'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('вес') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Цена
                    <input type="text" class="form-control <?= ($validation->hasError('цена')) ? 'is-invalid' : ''; ?>"
                           name="цена"
                           value="<?= old('цена'); ?>"/>
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('цена') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name">Изображение</label>
                <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
                <div class="invalid-feedback">
                    <?= $validation->getError('picture') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">или введите URL:
                    <input type="text" class="form-control <?= ($validation->hasError('картинка')) ? 'is-invalid' : ''; ?>"
                           name="картинка"
                           value="<?= old('картинка'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('картинка') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Цвет
                    <input type="text" class="form-control <?= ($validation->hasError('цвет')) ? 'is-invalid' : ''; ?>"
                           name="цвет"
                           value="<?= old('цвет'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('цвет') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Страна
                    <input type="text" class="form-control <?= ($validation->hasError('страна')) ? 'is-invalid' : ''; ?>"
                           name="страна"
                           value="<?= old('страна'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('страна') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Способ применения
                    <textarea type="text"
                              class="form-control <?= ($validation->hasError('способ_применения')) ? 'is-invalid' : ''; ?>"
                              name="способ_применения"><?= old('способ_применения'); ?></textarea>
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('способ_применения') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Состав
                    <textarea type="text" class="form-control <?= ($validation->hasError('состав')) ? 'is-invalid' : ''; ?>"
                              name="состав"><?= old('состав'); ?></textarea>
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('состав') ?>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="w-100">Объём
                    <input type="text" class="form-control <?= ($validation->hasError('объём')) ? 'is-invalid' : ''; ?>"
                           name="объём"
                           value="<?= old('объём'); ?>">
                </label>
                <div class="invalid-feedback">
                    <?= $validation->getError('объём') ?>
                </div>
            </div>



            <div class="form-group d-flex justify-content-center">
                <button type="submit" class="btn btn-outline-dark" name="submit">Создать</button>
            </div>
        </form>

    </div>
<?= $this->endSection() ?>