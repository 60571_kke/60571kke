<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="home_page">
    <div class="container h-100 d-flex align-items-center text-center">
        <div class="home_page_block col-12">
            <h4 class="my-4 text-white">В приложении предоставлен каталог парфюмерии, декоративной косметики, средств по уходу за волосами, лицом и телом</h4>

            <?php if (!$ionAuth->loggedIn()): ?>
                <a type="button" class="btn btn-outline-light" href="/auth/login"><p class="lead m-0">Войти</p></a>
            <?php else: ?>
                <a type="button" class="btn btn-outline-light" href="/auth/logout"><p class="lead m-0">Выйти</p></a>
            <?php endif ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row d-flex justify-content-center my-5">
        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                  <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308136.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Макияж</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308098.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Уход за лицом</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308104.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Уход за волосами</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308107.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Уход за телом</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308161.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Парфюмерия</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308154.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Маникюр и педикюр</div>
        </div>

        <div class="col-6 col-md-4 col-lg-3">
            <div class="d-flex justify-content-center mt-5 mb-3">
                <div class="container_img">
                    <img class="m-3" src="https://www.flaticon.com/svg/static/icons/svg/2308/2308148.svg" alt="" height="90">
                </div>
            </div>
            <div class="text-center">Гигиена полости рта</div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

